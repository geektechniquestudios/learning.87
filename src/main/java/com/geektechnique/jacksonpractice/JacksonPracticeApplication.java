package com.geektechnique.jacksonpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(JacksonPracticeApplication.class, args);
    }

}
